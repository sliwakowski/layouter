
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Cartography
#define COCOAPODS_POD_AVAILABLE_Cartography
#define COCOAPODS_VERSION_MAJOR_Cartography 0
#define COCOAPODS_VERSION_MINOR_Cartography 5
#define COCOAPODS_VERSION_PATCH_Cartography 0

// Haneke
#define COCOAPODS_POD_AVAILABLE_Haneke
#define COCOAPODS_VERSION_MAJOR_Haneke 1
#define COCOAPODS_VERSION_MINOR_Haneke 0
#define COCOAPODS_VERSION_PATCH_Haneke 1

