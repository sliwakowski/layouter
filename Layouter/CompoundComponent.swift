//
//  CompoundComponent.swift
//  Components
//
//  Created by Adam Śliwakowski on 25.04.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import Cartography

public class CompoundComponent: Component {

    var childComponents : [Component] = []
    var orientation : ComponentOrientation = .Horizontal
    
    public init(components: [Component], orientation: ComponentOrientation) {
        super.init(frame: CGRectMake(0, 0, 0, 0))
        self.orientation = orientation
        self.attributes = ComponentAttributes()
        addComponents(components)
    }
    
    public init(components: [Component], orientation: ComponentOrientation, attributes: ComponentAttributes) {
        super.init(attributes: attributes)
        self.orientation = orientation
        self.attributes = attributes
        addComponents(components)
    }
    
    
    public func addComponents(components: [Component]) {
        for (index, childComponent) in enumerate(components) {
            childComponent.setupComponent(self.attributes.data)
            self.childComponents.append(childComponent)
            self.addSubview(childComponent)
        }
        injectDataToChildComponents()
        layoutComponents()
    }
    
    public func injectDataToChildComponents() {
        let data = self.attributes.data
        if count(data.allKeys)>0 {
            for (index, childComponent) in enumerate(childComponents) {
                childComponent.attributes.data = data
                childComponent.setupComponent(self.attributes.data)
                
            }
        }
    }
    
    public override func setupComponent(data: NSDictionary) {
        super.setupComponent(data)
        injectDataToChildComponents()
    }
    
    func layoutComponents() {
        constrain(childComponents) { layoutViews in
            for (index, aView) in enumerate(layoutViews) {
                
                var actualComponent = self.childComponents[index]
                var actualComponentAttributes = actualComponent.attributes
                
                if index == 0 {
                    aView.left == aView.superview!.left + actualComponentAttributes.insets.left
                    aView.top == aView.superview!.top + actualComponentAttributes.insets.top
                }
                else {
                    var previousComponent = self.childComponents[index-1]
                    var previousComponentAttributes = previousComponent.attributes
                    switch self.orientation {
                    case .Horizontal:
                        aView.left == layoutViews[index - 1].right  + actualComponentAttributes.insets.left + previousComponentAttributes.insets.right
                        aView.top == aView.superview!.top + actualComponentAttributes.insets.top
                    case .Vertical:
                        aView.left == aView.superview!.left + actualComponentAttributes.insets.left
                        aView.top == layoutViews[index - 1].bottom + actualComponentAttributes.insets.top + previousComponentAttributes.insets.bottom
                    }
                }
                
                if !(actualComponent is CompoundComponent) {
                    aView.width == actualComponent.bounds.size.width
                    aView.height == actualComponent.bounds.size.height
                }
            }
        }
    }
    
    public func getHeight()->CGFloat {
        if(self.orientation == .Horizontal){
            return self.calculateMaxHeight()
        } else {
            return self.calculateHeight()
        }
    }
    
    public func getWidth()->CGFloat {
        if(self.orientation == .Horizontal){
            return self.calculateWidth()
        } else {
            return self.calculateMaxWidth()
        }
    }
    
    private func calculateHeight()->CGFloat {
        var height = CGFloat(0)
        for (index, childComponent) in enumerate(self.childComponents) {
            height += childComponent.frame.size.height
            var childComponentAttributes = childComponent.attributes
            height += childComponentAttributes.insets.top + childComponentAttributes.insets.bottom
        }
        return CGFloat(height)
    }
    
    private func calculateMaxHeight()->CGFloat {
        var height = CGFloat(0)
        for (index, childComponent) in enumerate(self.childComponents) {
            if(childComponent.frame.size.height>height) {
                height = childComponent.frame.size.height
                var childComponentAttributes = childComponent.attributes
                height += childComponentAttributes.insets.top + childComponentAttributes.insets.bottom
            }
        }
        return height
    }
    
    private func calculateWidth()->CGFloat {
        var width = CGFloat(0)
        for (index, childComponent) in enumerate(self.childComponents) {
            width += childComponent.frame.size.width
            var childComponentAttributes = childComponent.attributes
            width += childComponentAttributes.insets.left + childComponentAttributes.insets.right
        }
        return CGFloat(width)
    }
    
    private func calculateMaxWidth()->CGFloat {
        var width = CGFloat(0)
        for (index, childComponent) in enumerate(self.childComponents) {
            if(childComponent.frame.size.width>width) {
                width = childComponent.frame.size.width
                var childComponentAttributes = childComponent.attributes
                width += childComponentAttributes.insets.left + childComponentAttributes.insets.right
            }
        }
        return width
    }
    
    public override func didMoveToSuperview() {
        constrain(self) { view in
            view.width  == self.getWidth()
            view.height == self.getHeight()
            
            if !(self.superview is Component), let sv = view.superview {
                
                view.left == sv.left
                view.top == sv.top
            }
            
        }
        UIView.animateWithDuration(0, animations: self.layoutIfNeeded)
    }
    
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
