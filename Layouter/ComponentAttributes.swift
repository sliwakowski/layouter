//
//  ComponentAttributes.swift
//  Components
//
//  Created by Adam Śliwakowski on 28.04.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit

public class ComponentAttributes : NSObject {
    
    var size:               CGSize = CGSizeMake(0, 0)
    var insets:             UIEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
    var backgroundColor:    UIColor = UIColor.clearColor()
    
    var borderColor:        CGColor = UIColor.lightGrayColor().CGColor
    var borderWidth:        CGFloat = CGFloat(0)
    var cornerRadius:       CGFloat = 0
    
    var data = [:]
    
    override init() {
        
    }
    
    init(size: CGSize) {
        self.size = size
    }
    
    init(size: CGSize, insets: UIEdgeInsets) {
        self.size = size
        self.insets = insets
    }
}

public class TextViewComponentAttributes : ComponentAttributes {
    
    var text: String = ""
    var textColor: UIColor = UIColor.blackColor()
    var font: UIFont = UIFont.systemFontOfSize(13.0)
}

public class ImageViewComponentAttributes : ComponentAttributes {
    
    var imageName: String?
    var imageUrl: String?
}

public class ButtonComponentAttributes : ComponentAttributes {
    
    var imageName: String = ""
    var tintColor: UIColor = UIColor.blackColor()
    var selector: String = ""
}
