//
//  Component.swift
//  Components
//
//  Created by Adam Śliwakowski on 23.04.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import Cartography

public enum ComponentOrientation {
    case Vertical
    case Horizontal
}

public class Component: UIView {
    
    var attributes : ComponentAttributes = ComponentAttributes()
    
    public init(attributes: ComponentAttributes) {
        super.init(frame:CGRectMake(0, 0, attributes.size.width, attributes.size.height))
        self.attributes = attributes
        
    }
    
    public func setupComponent(data: NSDictionary) {
        self.backgroundColor = attributes.backgroundColor
        self.layer.borderWidth = attributes.borderWidth
        self.layer.borderColor = attributes.borderColor
        self.layer.cornerRadius = attributes.cornerRadius
        attributes.data = data
    }
    
    public func parentController() -> UIViewController {
        var object : AnyObject = self.nextResponder()!
        while( !(object.isKindOfClass(UIViewController)) ) {
            object = object.nextResponder()!
        }
        return object as! UIViewController;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    public class func createCard(index: Int) -> Component {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        var model : NSDictionary = defaults.objectForKey("model") as! NSDictionary
        
        var textAttributes = TextViewComponentAttributes()
        textAttributes.size = CGSizeMake(180, 25)
        //textAttributes.text = "COŚ tam cos tam cos tam . . . . ."
        textAttributes.text = "{title}"
        textAttributes.textColor = UIColor(rgba: "#2b2b38")
        textAttributes.font = UIFont.boldSystemFontOfSize(13.0)
        textAttributes.insets = UIEdgeInsetsMake(10,10,0,0)
        
        //var textAttributes2 = textAttributes
        //textAttributes2.insets = UIEdgeInsetsMake(0,10,0,0)
        
        
        var componentAttributes = ComponentAttributes()
        componentAttributes.borderWidth = CGFloat(0.5)
        componentAttributes.insets = UIEdgeInsetsMake(10,8,0,0)
        componentAttributes.size = CGSizeMake(0, 0)
        componentAttributes.cornerRadius = CGFloat(2.0)
        componentAttributes.backgroundColor = UIColor(rgba: "#FFFFFF")
        componentAttributes.borderColor = UIColor(rgba: "#d6d7db").CGColor
        componentAttributes.borderWidth = CGFloat(1.0)
        componentAttributes.data = model
        
        var imageViewAttributes = ImageViewComponentAttributes()
        imageViewAttributes.size = CGSizeMake(280, 200)
        //imageViewAttributes.imageName = "{imageName}"
        //imageViewAttributes.cornerRadius = CGFloat(20)
        imageViewAttributes.insets = UIEdgeInsetsMake(10,10,10,10)
        imageViewAttributes.imageUrl = "{imageUrl}"
        
        var smallImageViewAttributes = ImageViewComponentAttributes()
        smallImageViewAttributes.size = CGSizeMake(40, 40)
        smallImageViewAttributes.imageName = "{imageName}"
        smallImageViewAttributes.cornerRadius = CGFloat(3.0)
        componentAttributes.backgroundColor = UIColor(rgba: "#FFFFFF")
        componentAttributes.borderColor = UIColor(rgba: "#d6d7db").CGColor
        smallImageViewAttributes.insets = UIEdgeInsetsMake(10,10,0,0)
        
        var buttonAttributes = ButtonComponentAttributes()
        buttonAttributes.size = CGSizeMake(20, 20)
        buttonAttributes.insets = UIEdgeInsetsMake(8,19,0,0)
        buttonAttributes.tintColor = UIColor(rgba: "#b6bbc1")
        buttonAttributes.imageName = "{icon}"
        buttonAttributes.selector = "notty"
        //buttonAttributes.backgroundColor = UIColor.redColor()
        
        
        return CompoundComponent(
            components: [
                CompoundComponent(components: [
                    ImageViewComponent(imageViewAttributes: smallImageViewAttributes),
                    CompoundComponent(
                        components: [
                            TextViewComponent(textViewAttributes: textAttributes),
                            //TextViewComponent(textViewAttributes: textAttributes2)
                        ],
                        orientation: .Vertical
                    ),
                    ButtonComponent(buttonAttributes: buttonAttributes)
                    ],
                    orientation: .Horizontal
                ),
                ImageViewComponent(imageViewAttributes: imageViewAttributes)
            ],
            orientation: .Vertical,
            attributes: componentAttributes
        )
        
        
        
    }
    
    public func getModelFromValue(value: String) -> String? {
        if (value.hasPrefix("{") && value.hasSuffix("}")) {
            var model = value
            model.removeAtIndex(model.endIndex.predecessor())
            model.removeAtIndex(model.startIndex)
            return model
        }
        return nil
    }
    
    public override init(frame: CGRect) {
        super.init(frame:frame)
    }
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
