//
//  ImageViewComponent.swift
//  Components
//
//  Created by Adam Śliwakowski on 01.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import Haneke

public class ImageViewComponent: Component {

    var imageView : UIImageView?
    var imageViewAttributes : ImageViewComponentAttributes?
    
    public init(imageViewAttributes: ImageViewComponentAttributes){
        super.init(attributes: imageViewAttributes)
        self.imageViewAttributes = imageViewAttributes
        self.layer.masksToBounds = true
        addImageViewToComponent()
    }
    
    func addImageViewToComponent() {
        imageView = UIImageView(frame: frame)
        imageView?.layer.cornerRadius = imageViewAttributes!.cornerRadius
        imageView?.layer.masksToBounds = true
        self.addSubview(imageView!)
    }
    
    public override func setupComponent(data: NSDictionary) {
        setImage(data)
    }
    
    private func setImage(data: NSDictionary) {
        if
            let imageNameModel = imageViewAttributes?.imageName,
            let model : String = getModelFromValue(imageNameModel),
            let imageName : String = data[model] as? String,
            let image = UIImage(named: imageName)
        {
            self.imageView?.image = image
        }
        else if
            let imageUrlModel = imageViewAttributes?.imageUrl,
            let model : String = getModelFromValue(imageUrlModel),
            let imageUrl : String = data[model] as? String,
            let imageURL : NSURL = NSURL(string: imageUrl),
            let imageView : UIImageView = self.imageView
        {
            imageView.hnk_setImageFromURL(imageURL, placeholder: UIImage(named: "mybaze.jpg"), success: nil, failure: nil)
        }
    }
    

    public required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
