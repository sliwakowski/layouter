//
//  ButtonComponent.swift
//  Components
//
//  Created by Adam Śliwakowski on 03.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit

class ButtonComponent: Component {
    
    var button : UIButton?
    var buttonAttributes : ButtonComponentAttributes?
    
    init(buttonAttributes: ButtonComponentAttributes) {
        super.init(attributes: buttonAttributes)
        self.attributes = buttonAttributes
        self.buttonAttributes = buttonAttributes
        addButtonToComponent()
    }
    
    func addButtonToComponent() {
        button = UIButton.buttonWithType(UIButtonType.Custom) as? UIButton
        button?.frame = self.frame
        self.addSubview(button!)
    }
    
    override func setupComponent(data: NSDictionary) {
        super.setupComponent(data)
        button?.tintColor = buttonAttributes?.tintColor
        button!.addTarget(self, action: NSSelectorFromString("buttonAction:"), forControlEvents: UIControlEvents.TouchUpInside)
        setButtonImage(data)
    }
    
    func buttonAction(sender: ButtonComponent) {
        println("action");
        var controller = parentController()
        var timer = NSTimer.scheduledTimerWithTimeInterval(0, target: controller, selector: Selector(buttonAttributes!.selector), userInfo: attributes.data, repeats: false)

        println("asd");
    }
    
    
    func setButtonImage(data: NSDictionary) {
        if
            let model : String = getModelFromValue(buttonAttributes!.imageName),
            let imageName : String = data[model] as? String {
            button?.setImage(
                UIImage(named: imageName)?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate),
                forState: UIControlState.Normal)
        } else {
            button?.setImage(
                UIImage(named: buttonAttributes!.imageName)?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate),
                forState: UIControlState.Normal)
        }
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
