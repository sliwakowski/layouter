//
//  TextViewComponent.swift
//  Components
//
//  Created by Adam Śliwakowski on 28.04.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit

public class TextViewComponent: Component {
    
    var textView : UITextView?
    
    public init(textViewAttributes: TextViewComponentAttributes) {
        super.init(attributes: textViewAttributes)
        textView = UITextView(frame: frame)
        attributes = textViewAttributes
        self.addSubview(textView!)
    }
    
    public override func setupComponent(data: NSDictionary) {
        super.setupComponent(data)
        var textViewAttributes : TextViewComponentAttributes = self.attributes as! TextViewComponentAttributes
        
        textView?.backgroundColor = UIColor.clearColor()
        textView?.editable = false
        textView?.scrollEnabled = false
        textView?.selectable = false
        textView?.contentInset = UIEdgeInsets(top: -8,left: -5,bottom: 0,right: 0)
        textView?.font = textViewAttributes.font
        textView?.textColor = textViewAttributes.textColor
        setText(textViewAttributes,data: data)
    }
    
    func setText(textViewAttributes: TextViewComponentAttributes, data: NSDictionary) {
        if let model : String = getModelFromValue(textViewAttributes.text), let value : String = data[model] as? String {
            textView?.text = value
        } else {
            textView?.text = textViewAttributes.text
        }
    }
    
    
    public required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
